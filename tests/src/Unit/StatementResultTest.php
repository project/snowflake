<?php

namespace Drupal\Tests\snowflake\Unit;

use Drupal\Component\Serialization\Json;
use Drupal\snowflake\StatementResult\ResultSet;
use Drupal\snowflake\StatementResult\ResultSetMetadata;
use Drupal\snowflake\StatementResult\ResultSetStats;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Psr7\Response;

/**
 * Covers all classes in Drupal\snowflake\StatementResult namespace.
 *
 * @group snowflake
 */
class StatementResultTest extends UnitTestCase {

  /**
   * Example body of a result set response.
   */
  protected array $resultSetBody;

  /**
   * Example result set response.
   */
  protected Response $resultSetResponse;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->resultSetBody = [
      'code' => '111201332',
      'message' => 'Query executed successfully',
      'statementHandle' => 'c91c32c9-812a-4b93-b847-6df2bcc38367',
      'createdOn' => time(),
      'sqlState' => '00000000',
      'statementStatusUrl' => 'https://www.example.com/status/c91c32c9-812a-4b93-b847-6df2bcc38367',
      'resultSetMetaData' => [
        'format' => 'jsonv2',
        'numRows' => '2',
        'partitionInfo' => [
          [
            'rowCount' => 2,
            'uncompressedSize' => 100,
            'compressedSize' => 75,
          ],
        ],
        'rowType' => [
          [
            'byteLength' => 100,
            'collation' => NULL,
            'database' => 'MY-DATABASE',
            'length' => 100,
            'name' => 'USER_ID',
            'nullable' => FALSE,
            'precision' => NULL,
            'scale' => NULL,
            'schema' => 'MY-SCHEMA',
            'table' => 'USERS',
            'type' => 'REAL',
          ],
          [
            'byteLength' => 100,
            'collation' => NULL,
            'database' => 'MY-DATABASE',
            'length' => 100,
            'name' => 'NAME',
            'nullable' => FALSE,
            'precision' => NULL,
            'scale' => NULL,
            'schema' => 'MY-SCHEMA',
            'table' => 'USERS',
            'type' => 'TEXT',
          ],
        ],
      ],
      'data' => [['1', 'Bill'], ['2', 'Bob']],
      'statementHandles' => ['c91c32c9-812a-4b93-b847-6df2bcc38367'],
      'stats' => [
        'numRowsDeleted' => 0,
        'numDuplicateRowsUpdated' => 0,
        'numRowsInserted' => 1,
        'numRowsUpdated' => 0,
      ],
    ];

    $this->resultSetResponse = new Response(
      200,
      ['Content-Type' => ['application/json']],
      Json::encode($this->resultSetBody)
    );

  }

  /**
   * Asserts functionality of result set columns metadata.
   */
  public function testColumnMetaData(): void {
    $result = new ResultSet($this->resultSetResponse);
    $columnMetadata = $result->getMetadata()->getColumnMetadata();
    $bodyColumnMetadata = $this->resultSetBody['resultSetMetaData']['rowType'][0];
    $this->assertEquals($bodyColumnMetadata['byteLength'], $columnMetadata[0]->getByteLength());
    $this->assertEquals($bodyColumnMetadata['collation'], $columnMetadata[0]->getCollation());
    $this->assertEquals($bodyColumnMetadata['database'], $columnMetadata[0]->getDatabase());
    $this->assertEquals($bodyColumnMetadata['length'], $columnMetadata[0]->getLength());
    $this->assertEquals($bodyColumnMetadata['name'], $columnMetadata[0]->getName());
    $this->assertFalse($columnMetadata[0]->isNullable());
    $this->assertEquals($bodyColumnMetadata['precision'], $columnMetadata[0]->getPrecision());
    $this->assertEquals($bodyColumnMetadata['scale'], $columnMetadata[0]->getScale());
    $this->assertEquals($bodyColumnMetadata['schema'], $columnMetadata[0]->getSchema());
    $this->assertEquals($bodyColumnMetadata['table'], $columnMetadata[0]->getTable());
    $this->assertEquals($bodyColumnMetadata['type'], $columnMetadata[0]->getType());
  }

  /**
   * Asserts functionality of result set result.
   */
  public function testResultSet(): void {
    $result = new ResultSet($this->resultSetResponse);
    $this->assertEquals($this->resultSetBody['createdOn'], $result->getCreatedOn());
    $this->assertJsonStringEqualsJsonString(
      Json::encode($this->resultSetBody['data']),
      Json::encode($result->getData())
    );
    $this->assertEquals($this->resultSetBody['statementHandles'], $result->getStatementHandles());
    $this->assertEquals($this->resultSetBody['statementStatusUrl'], $result->getStatementStatusUrl());
    $this->assertInstanceOf(ResultSetMetadata::class, $result->getMetadata());
    $this->assertInstanceOf(ResultSetStats::class, $result->getStats());
  }

  /**
   * Asserts functionality of result set metadata.
   */
  public function testResultSetMetadata(): void {
    $result = new ResultSet($this->resultSetResponse);
    $metadata = $result->getMetadata();
    $bodyMetadata = $this->resultSetBody['resultSetMetaData'];
    $this->assertEquals($bodyMetadata['format'], $metadata->getFormat());
    $this->assertEquals($bodyMetadata['numRows'], $metadata->getNumRows());
    $this->assertJsonStringEqualsJsonString(
      Json::encode($bodyMetadata['partitionInfo']),
      Json::encode($metadata->getPartitionInfo())
    );
  }

  /**
   * Asserts functionality of result set stats.
   */
  public function testResultSetStats(): void {
    $result = new ResultSet($this->resultSetResponse);
    $stats = $result->getStats();
    $bodyStats = $this->resultSetBody['stats'];
    $this->assertEquals($bodyStats['numRowsDeleted'], $stats->getDeleted());
    $this->assertEquals($bodyStats['numDuplicateRowsUpdated'], $stats->getDuplicateUpdated());
    $this->assertEquals($bodyStats['numRowsInserted'], $stats->getInserted());
    $this->assertEquals($bodyStats['numRowsUpdated'], $stats->getUpdated());
  }

  /**
   * Asserts functionality of base statement result.
   */
  public function testStatementResultBase(): void {
    $result = new ResultSet($this->resultSetResponse);
    $this->assertEquals($this->resultSetBody['code'], $result->getCode());
    $this->assertEquals($this->resultSetBody['message'], $result->getMessage());
    $this->assertJsonStringEqualsJsonString(
      Json::encode($this->resultSetBody),
      Json::encode($result->getRawResults())
    );
    $this->assertEquals($this->resultSetResponse, $result->getResponse());
    $this->assertEquals($this->resultSetBody['statementHandle'], $result->getStatementHandle());
  }

}
