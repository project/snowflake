<?php

namespace Drupal\Tests\snowflake\Unit;

use Drupal\snowflake\Statement\Statement;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\snowflake\Statement\Statement
 *
 * @group snowflake
 */
class StatementTest extends UnitTestCase {

  /**
   * Assert that the class constructor works.
   */
  public function testConstructor(): void {
    $statement = new Statement('SELECT FIRST_NAME, LAST_NAME FROM CONTACTS LIMIT 10');
    $this->assertInstanceOf(Statement::class, $statement);
  }

  /**
   * Assert that getters work.
   */
  public function testGetters(): void {
    $query = 'SELECT FIRST_NAME, LAST_NAME FROM CONTACTS WHERE USER_ID=?';
    $binding = ['type' => 'TEXT', 'value' => '1234567890'];
    $statement = Statement::create($query)
      ->addBinding(...$binding);
    $this->assertEquals($query, $statement->getStatement());
    $this->assertEquals($query, (string) $statement);
    $this->assertEquals([$binding], $statement->getBindings());
  }

  /**
   * Assert that supported bindings work.
   *
   * @dataProvider supportedBindingTypes
   */
  public function testSupportedBindings(string $type, mixed $value, string $expected): void {
    $statement = Statement::create('SELECT ID FROM TABLE WHERE COLUMN_VALUE=?')->addBinding($type, $value);
    $this->assertSame($expected, $statement->getBindings()[0]['value']);
  }

  /**
   * Assert that unsupported bindings do not work.
   */
  public function testUnsupportedBinding(): void {
    $statement = Statement::create('SELECT ID FROM TABLE WHERE COLUMN_VALUE=?');
    $this->expectException(\InvalidArgumentException::class);
    $statement->addBinding('FLOAT', 1.2345);
  }

  /**
   * Provides data for testBindings().
   */
  public function supportedBindingTypes(): array {
    $date = new \DateTime();
    $timestamp = $date->getTimestamp();
    return [
      ['BINARY', 'HELLO', 'HELLO'],
      ['BOOLEAN', TRUE, 'true'],
      ['BOOLEAN', FALSE, 'false'],
      ['DATE', $date->format('Y-m-d'), $date->format('Y-m-d')],
      ['FIXED', 1234, '1234'],
      ['REAL', 1.234, '1.234'],
      ['TIME', $timestamp, (string) $timestamp],
      ['TIMESTAMP_LTZ', $timestamp, (string) $timestamp],
      ['TIMESTAMP_NTZ', $timestamp, (string) $timestamp],
      ['TIMESTAMP_TZ', $timestamp, (string) $timestamp],
    ];
  }

}
