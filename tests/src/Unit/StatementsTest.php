<?php

namespace Drupal\Tests\snowflake\Unit;

use Drupal\Component\Uuid\Php;
use Drupal\snowflake\Statement\Statement;
use Drupal\snowflake\Statement\Statements;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\snowflake\Statement\Statements
 *
 * @group snowflake
 */
class StatementsTest extends UnitTestCase {

  /**
   * Assert that the class constructor works.
   */
  public function testConstructor(): void {
    $statement = new Statement('SELECT FIRST_NAME, LAST_NAME FROM CONTACTS LIMIT 10');
    $statements = new Statements($statement);
    $this->assertInstanceOf(Statements::class, $statements);
  }

  /**
   * Assert that adding statements works.
   */
  public function testAddStatement(): void {
    $statement1 = new Statement('SELECT FIRST_NAME, LAST_NAME FROM CONTACTS LIMIT 10');
    $statement2 = new Statement('SELECT USER FROM USERS LIMIT 20');

    $statements = new Statements();
    $statements->addStatement($statement1);
    $this->assertEquals((string) $statement1, (string) $statements);
    $statements->addStatement($statement2);
    $this->assertEquals("$statement1; $statement2", (string) $statements);
    $this->assertEquals(2, count($statements));

    $binding = ['type' => 'TEXT', 'value' => 'JIM'];
    $statement3 = Statement::create('SELECT USER FROM USERS WHERE NAME=?')->addBinding(...$binding);
    $statements->addStatement($statement3);
    $this->assertEquals([1 => $binding], $statements->getBindings());
  }

  /**
   * Assert that basic setters and getters work.
   */
  public function testSettersAndGetters(): void {
    $statements = new Statements();
    $statements->setAsync(TRUE);
    $this->assertTrue($statements->isAsync());
    $statements->setNullable(FALSE);
    $this->assertFalse($statements->isNullable());
    $uuid = (new Php())->generate();
    $statements->setId($uuid);
    $this->assertEquals($uuid, $statements->getId());
    $this->expectException(\InvalidArgumentException::class);
    $this->expectExceptionMessage("ID NOT A UUID is not a valid UUID. Statements ID must be a valid UUID.");
    $statements->setId('NOT A UUID');
  }

  /**
   * Assert that supported parameters work.
   *
   * @dataProvider supportedParameters
   */
  public function testSupportedParameters(string $parameter, string $value): void {
    $statements = new Statements();
    $statements->addParameter($parameter, $value);
    $this->assertSame([strtoupper($parameter) => $value], $statements->getParameters());
  }

  /**
   * Assert that unsupported parameters do not work.
   */
  public function testUnsupportedParameter(): void {
    $statements = new Statements();
    $this->expectException(\InvalidArgumentException::class);
    $statements->addParameter('NOT A PARAMETER', 'HELLO!');
  }

  /**
   * Provides data for testBindings().
   */
  public function supportedParameters(): array {
    return [
      ['BINARY_OUTPUT_FORMAT', 'HEX'],
      ['BiNaRy_OuTPuT_FoRmAt', 'HEX'],
      ['DATE_OUTPUT_FORMAT', 'YYYY-MM-DD'],
      ['query_tag', 'my-query-tag'],
      ['TIME_OUTPUT_FORMAT', 'HH:mm:ss'],
      ['TIMESTAMP_LTZ_OUTPUT_format', 'YYYY-MM-DD:HH:mm:ss.x'],
      ['TIMESTAMP_NTZ_OUTPUT_FORMAT', 'YYYY-MM-DD:HH:mm:ss.x'],
      ['TIMESTAMP_OUTPUT_FORMAT', 'YYYY-MM-DD:HH:mm:ss.x'],
      ['TIMESTAMP_tz_OUTPUT_FORMAT', 'YYYY-MM-DD:mm:MM:ss.x'],
      ['timezone', 'America/Los_Angeles'],
    ];
  }

  /**
   * Assert that supported settings work.
   *
   * @dataProvider supportedSettings
   */
  public function testSupportedSettings(string $setting, string $value): void {
    $statements = new Statements();
    $statements->addSetting($setting, $value);
    $this->assertSame([strtolower($setting) => $value], $statements->getSettings());
  }

  /**
   * Assert that unsupported parameters do not work.
   */
  public function testUnsupportedSetting(): void {
    $statements = new Statements();
    $this->expectException(\InvalidArgumentException::class);
    $statements->addSetting('NOT A SETTING', 'GOODBYE!');
  }

  /**
   * Provides data for testBindings().
   */
  public function supportedSettings(): array {
    return [
      ['timeout', '60'],
      ['DATABASE', 'MY_DATABASE'],
      ['schema', 'MY_SCHEMA'],
      ['wareHOUSE', 'MY_WAREHOUSE'],
      ['Role', 'USER_ROLE'],
    ];
  }

}
