<?php

namespace Drupal\Tests\snowflake\Unit;

use Drupal\snowflake\Token\Token;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\snowflake\Token\Token
 *
 * @group snowflake
 */
class TokenTest extends UnitTestCase {

  /**
   * Assert that the class constructor works.
   */
  public function testConstructor(): void {
    $token = new Token('ACCESS_TOKEN', -1, 'keypair');
    $this->assertInstanceOf(Token::class, $token);

    $this->expectException(\InvalidArgumentException::class);
    $this->expectExceptionMessage('Unexpected token type: invalid');
    new Token('ACCESS_TOKEN', -1, 'invalid');
  }

  /**
   * Assert that setters work.
   */
  public function testSetters(): void {
    $access_token = 'NEW ACCESS_TOKEN';
    $eol = 100;
    $token = new Token('ACCESS_TOKEN', -1, 'keypair');
    $token->setAccessToken($access_token);
    $this->assertEquals($access_token, $token->getAccessToken());
    $token->setEndOfLife($eol);
    $this->assertEquals($eol, $token->getEndOfLife());
  }

  /**
   * Assert that getters work.
   */
  public function testGetters(): void {
    $access_token = 'ACCESS_TOKEN';
    $eol = -1;
    $token = new Token($access_token, $eol, 'keypair');
    $this->assertEquals($access_token, $token->getAccessToken());
    $this->assertEquals($eol, $token->getEndOfLife());
    $this->assertEquals('KEYPAIR_JWT', $token->getType());
  }

  /**
   * Assert that expiration check works.
   */
  public function testExpired(): void {
    $time = time();
    $token = new Token('ACCESS TOKEN', -1, 'keypair');
    $this->assertTrue($token->isExpired());
    $token->setEndOfLife($time + 100);
    $this->assertFalse($token->isExpired());
  }

}
