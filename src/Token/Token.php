<?php

namespace Drupal\snowflake\Token;

/**
 * Snowflake auth token.
 */
class Token implements TokenInterface {

  /**
   * Access token.
   */
  protected string $accessToken;

  /**
   * Access token end of life (unix time).
   */
  protected int $endOfLife;

  /**
   * Access token type.
   */
  protected string $type;

  /**
   * Constructs a Token instance.
   */
  public function __construct(string $access_token, int $end_of_life, string $type) {
    $this->setAccessToken($access_token);
    $this->setEndOfLife($end_of_life);
    $this->type = match($type) {
      'keypair' => 'KEYPAIR_JWT',
      'oauth' => 'OAUTH',
      default => throw new \InvalidArgumentException("Unexpected token type: {$type}")
    };
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken(): string {
    return $this->accessToken;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndOfLife(): int {
    return $this->endOfLife;
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function isExpired(): bool {
    return $this->getEndOfLife() <= time();
  }

  /**
   * {@inheritdoc}
   */
  public function setAccessToken(string $access_token) {
    $this->accessToken = $access_token;
  }

  /**
   * {@inheritdoc}
   */
  public function setEndOfLife(int $end_of_life) {
    $this->endOfLife = $end_of_life;
  }

}
