<?php

namespace Drupal\snowflake\Token;

/**
 * Base Snowflake token interface.
 */
interface TokenInterface {

  /**
   * Gets the Snowflake SQL API access token.
   */
  public function getAccessToken(): string;

  /**
   * Gets the access token expire time (unix time).
   */
  public function getEndOfLife(): int;

  /**
   * Gets the token type.
   *
   * Used in the `X-Snowflake-Authorization-Token-Type` for SQL API queries.
   */
  public function getType(): string;

  /**
   * Indicates if the token is currently expired.
   */
  public function isExpired(): bool;

  /**
   * Sets the Snowflake SQL API access token.
   */
  public function setAccessToken(string $access_token);

  /**
   * Sets the access token expire time (unix time).
   */
  public function setEndOfLife(int $end_of_life);

}
