<?php

namespace Drupal\snowflake\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a general settings form for Snowflake.
 *
 * @package Drupal\snowflake\Form
 */
final class SettingsForm extends SettingsFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'snowflake_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigName(): string {
    return 'snowflake.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config($this->getConfigName());

    $form['statements_defaults_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Default Statements Settings'),
      '#description' => $this->t('Default settings for statements API queries. See <a href="@url">Snowflake SQL API Reference > Body of the POST Request > Fields</a> for details.', [
        '@url' => 'https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#fields',
      ]),
      '#open' => TRUE,
    ];
    $form['statements_defaults_settings']['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('Timeout in seconds for statement execution. To set the timeout to the maximum value (604800 seconds), set timeout to 0.'),
      '#default_value' => $config->get('statements_defaults.settings.timeout'),
      '#field_suffix' => 'seconds',
      '#min' => 0,
      '#max' => 604800,
      '#step' => 1,
      '#disabled' => $this->configIsOverridden('statements_defaults.settings.timeout'),
    ];
    $form['statements_defaults_settings']['database'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database'),
      '#description' => $this->t('Database in which the statement should be executed.'),
      '#default_value' => $config->get('statements_defaults.settings.database'),
      '#disabled' => $this->configIsOverridden('statements_defaults.settings.database'),
    ];
    $form['statements_defaults_settings']['schema'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Schema'),
      '#description' => $this->t('Schema in which the statement should be executed.'),
      '#default_value' => $config->get('statements_defaults.settings.schema'),
      '#disabled' => $this->configIsOverridden('statements_defaults.settings.schema'),
    ];
    $form['statements_defaults_settings']['warehouse'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Warehouse'),
      '#description' => $this->t('Warehouse to use when executing the statement.'),
      '#default_value' => $config->get('statements_defaults.settings.warehouse'),
      '#disabled' => $this->configIsOverridden('statements_defaults.settings.warehouse'),
    ];
    $form['statements_defaults_settings']['role'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Role'),
      '#description' => $this->t('Role to use when executing the statement.'),
      '#default_value' => $config->get('statements_defaults.settings.role'),
      '#disabled' => $this->configIsOverridden('statements_defaults.settings.role'),
    ];

    $form['statements_defaults_parameters'] = [
      '#type' => 'details',
      '#title' => $this->t('Default Statements Session Parameters'),
      '#description' => $this->t('Default session parameters for statements API queries. See <a href="@url">Snowflake SQL API Reference > Body of the POST Request > Sessions Parameters</a> for details.', [
        '@url' => 'https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#statements-parameters',
      ]),
      '#open' => FALSE,
    ];
    $form['statements_defaults_parameters']['BINARY_OUTPUT_FORMAT'] = [
      '#type' => 'select',
      '#title' => $this->t('Binary output format'),
      '#options' => ['HEX', 'BASE64'],
      '#empty_option' => $this->t('- Default -'),
      '#description' => $this->t('Specifies format for VARCHAR values returned as output by BINARY-to-VARCHAR conversion functions.'),
      '#default_value' => $config->get('statements_defaults.parameters.BINARY_OUTPUT_FORMAT'),
      '#disabled' => $this->configIsOverridden('statements_defaults.parameters.BINARY_OUTPUT_FORMAT'),
    ];
    $form['statements_defaults_parameters']['DATE_OUTPUT_FORMAT'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date output format'),
      '#description' => $this->t('Specifies the display format for the DATE data type.'),
      '#default_value' => $config->get('statements_defaults.parameters.DATE_OUTPUT_FORMAT'),
      '#disabled' => $this->configIsOverridden('statements_defaults.parameters.DATE_OUTPUT_FORMAT'),
    ];
    $form['statements_defaults_parameters']['QUERY_TAG'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Query tag'),
      '#description' => $this->t('Query tag that you want to associate with the SQL statement.'),
      '#maxlength' => 2000,
      '#default_value' => $config->get('statements_defaults.parameters.QUERY_TAG'),
      '#disabled' => $this->configIsOverridden('statements_defaults.parameters.QUERY_TAG'),
    ];
    $form['statements_defaults_parameters']['TIMEZONE'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Timezone'),
      '#description' => $this->t('Time zone to use when executing the statement.'),
      '#default_value' => $config->get('statements_defaults.parameters.TIMEZONE'),
      '#disabled' => $this->configIsOverridden('statements_defaults.parameters.TIMEZONE'),
    ];
    $form['statements_defaults_parameters']['TIME_OUTPUT_FORMAT'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Time output format'),
      '#description' => $this->t('Specifies the display format for the <code>TIME</code> data type.'),
      '#default_value' => $config->get('statements_defaults.parameters.TIME_OUTPUT_FORMAT'),
      '#disabled' => $this->configIsOverridden('statements_defaults.parameters.TIME_OUTPUT_FORMAT'),
    ];
    $form['statements_defaults_parameters']['TIMESTAMP_LTZ_OUTPUT_FORMAT'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Timestamp (with local time zone) format'),
      '#description' => $this->t('Specifies the display format for the <code>TIMESTAMP_LTZ</code> data type.'),
      '#default_value' => $config->get('statements_defaults.parameters.TIMESTAMP_LTZ_OUTPUT_FORMAT'),
      '#disabled' => $this->configIsOverridden('statements_defaults.parameters.TIMESTAMP_LTZ_OUTPUT_FORMAT'),
    ];
    $form['statements_defaults_parameters']['TIMESTAMP_NTZ_OUTPUT_FORMAT'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Timestamp (without local time zone) format'),
      '#description' => $this->t('Specifies the display format for the <code>TIMESTAMP_NTZ</code> data type.'),
      '#default_value' => $config->get('statements_defaults.parameters.TIMESTAMP_NTZ_OUTPUT_FORMAT'),
      '#disabled' => $this->configIsOverridden('statements_defaults.parameters.TIMESTAMP_NTZ_OUTPUT_FORMAT'),
    ];
    $form['statements_defaults_parameters']['TIMESTAMP_OUTPUT_FORMAT'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Timestamp format'),
      '#description' => $this->t('Specifies the display format for the <code>TIMESTAMP</code> data type.'),
      '#default_value' => $config->get('statements_defaults.parameters.TIMESTAMP_OUTPUT_FORMAT'),
      '#disabled' => $this->configIsOverridden('statements_defaults.parameters.TIMESTAMP_OUTPUT_FORMAT'),
    ];
    $form['statements_defaults_parameters']['TIMESTAMP_TZ_OUTPUT_FORMAT'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Timestamp (with time zone) format'),
      '#description' => $this->t('Specifies the display format for the <code>TIMESTAMP_TZ</code> data type.'),
      '#default_value' => $config->get('statements_defaults.parameters.TIMESTAMP_TZ_OUTPUT_FORMAT'),
      '#disabled' => $this->configIsOverridden('statements_defaults.parameters.TIMESTAMP_TZ_OUTPUT_FORMAT'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config($this->getConfigName());
    foreach (['timeout', 'database', 'schema', 'warehouse', 'role'] as $key) {
      if (!$this->configIsOverridden("statements_defaults.settings.$key")) {
        $config->set("statements_defaults.settings.$key", $form_state->getValue($key));
      }
    }
    $statements_defaults_parameters = [
      'BINARY_OUTPUT_FORMAT',
      'DATE_OUTPUT_FORMAT',
      'QUERY_TAG',
      'TIME_OUTPUT_FORMAT',
      'TIMESTAMP_LTZ_OUTPUT_FORMAT',
      'TIMESTAMP_NTZ_OUTPUT_FORMAT',
      'TIMESTAMP_OUTPUT_FORMAT',
      'TIMESTAMP_TZ_OUTPUT_FORMAT',
      'TIMEZONE',
    ];
    foreach ($statements_defaults_parameters as $key) {
      if (!$this->configIsOverridden("statements_defaults.parameters.$key")) {
        $config->set("statements_defaults.parameters.$key", $form_state->getValue($key));
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
