<?php

namespace Drupal\snowflake\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a general authentication settings form for Snowflake.
 *
 * @package Drupal\snowflake\Form
 */
final class AuthSettingsForm extends SettingsFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'snowflake_auth_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigName(): string {
    return 'snowflake.auth';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config($this->getConfigName());
    $form['account_identifier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account identifier'),
      '#description' => $this->t('Snowflake <a href="@account_identifier_url">account identifier</a>. If you are using the <a href="@account_locator_url">account locator</a>, exclude any region information from the account locator.', [
        '@account_identifier_url' => 'https://docs.snowflake.com/en/user-guide/admin-account-identifier.html',
        '@account_locator_url' => 'https://docs.snowflake.com/en/user-guide/admin-account-identifier.html#label-account-locator',
      ]),
      '#default_value' => $config->get('account_identifier'),
      '#required' => TRUE,
      '#disabled' => $this->configIsOverridden('account_identifier'),
    ];
    $form['method'] = [
      '#type' => 'select',
      '#title' => $this->t('Authentication method'),
      '#description' => $this->t('Default authentication method to use for queries.'),
      '#options' => [
        'key_pair' => $this->t('Key Pair'),
      ],
      '#required' => TRUE,
      '#default_value' => $config->get('method'),
      '#disabled' => $this->configIsOverridden('method'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $config = $this->config($this->getConfigName());

    if ($form_state->getValue('method') === 'key_pair') {
      // Check for dependencies.
      $installed = $this->checkKeyPairDependencies();
      if (!$installed) {
        $form_state->setErrorByName('method', $this->t('Missing requirements for selected authentication method.'));
      }

      // Check for required configuration.
      foreach (['user', 'private_key'] as $key) {
        if (!$config->get("key_pair.$key")) {
          $form_state->setErrorByName('method', $this->t('The Key Pair @key has not been configured. Visit the <a href="@url">Key Pair settings</a> to add it.', [
            '@url' => Url::fromRoute('snowflake.auth_config.key_pair')->toString(),
            '@key' => match ($key) {
              'private_key' => 'private key',
              default => $key
            },
          ]));
        }
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config($this->getConfigName());
    foreach (['account_identifier', 'method'] as $key) {
      if (!$this->configIsOverridden($key)) {
        $config->set($key, $form_state->getValue($key));
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
