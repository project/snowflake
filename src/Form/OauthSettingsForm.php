<?php

namespace Drupal\snowflake\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an OAuth authorization settings form.
 *
 * @package Drupal\snowflake\Form
 */
final class OauthSettingsForm extends SettingsFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'snowflake_auth_config_oauth';
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigName(): string {
    return 'snowflake.auth';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config($this->getConfigName());

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('OAuth integration ID (provided by Snowflake when the client is registered).'),
      '#default_value' => $config->get('oauth.client_id'),
      '#required' => TRUE,
      '#disabled' => $this->configIsOverridden('oauth.client_id'),
    ];
    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#description' => $this->t('OAuth integration secret (provided by Snowflake when the client is registered)'),
      '#default_value' => $config->get('oauth.client_secret'),
      '#required' => TRUE,
      '#disabled' => $this->configIsOverridden('oauth.client_secret'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config($this->getConfigName());
    foreach (['client_id', 'client_secret'] as $key) {
      if (!$this->configIsOverridden("oauth.$key")) {
        $config->set("oauth.$key", $form_state->getValue($key));
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
