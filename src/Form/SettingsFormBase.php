<?php

namespace Drupal\snowflake\Form;

use Composer\InstalledVersions;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Base settings form for Snowflake.
 *
 * @package Drupal\snowflake\Form
 */
abstract class SettingsFormBase extends ConfigFormBase {

  /**
   * Gets the name of the config the form modifies.
   */
  abstract protected function getConfigName(): string;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [$this->getConfigName()];
  }

  /**
   * Check if a config variable is overridden by local settings.
   */
  protected function configIsOverridden(string $setting_name): bool {
    return $this->configFactory->getEditable($this->getConfigName())->get($setting_name)
      != $this->configFactory->get($this->getConfigName())->get($setting_name);
  }

  /**
   * Checks in f the required firebase/jwt-php library is installed.
   *
   * Also adds a warning if it is not found.
   */
  protected function checkKeyPairDependencies(): bool {
    if (!InstalledVersions::isInstalled('firebase/php-jwt')) {
      $this->messenger()->addWarning($this->t('The <a href="@url">firebase/php-jwt</a> library must be installed to use Key Pair authentication. Run <code>composer require firebase/php-jwt</code> to install it.', [
        '@url' => 'https://packagist.org/packages/firebase/php-jwt',
      ]));
      return FALSE;
    }
    return TRUE;
  }

}
