<?php

namespace Drupal\snowflake\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Key-Pair authorization settings form.
 *
 * @package Drupal\snowflake\Form
 */
final class KeyPairSettingsForm extends SettingsFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'snowflake_auth_config_key_pair';
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigName(): string {
    return 'snowflake.auth';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config($this->getConfigName());

    $form['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Snowflake user'),
      '#default_value' => $config->get('key_pair.user'),
      '#required' => TRUE,
      '#disabled' => $this->configIsOverridden('key_pair.user'),
    ];
    $form['private_key'] = [
      '#title' => 'Private Key',
      '#type' => 'key_select',
      '#empty_option' => $this->t('- Select -'),
      '#key_filters' => ['type' => 'authentication'],
      '#default_value' => $config->get('key_pair.private_key'),
      '#required' => TRUE,
      '#disabled' => $this->configIsOverridden('key_pair.private_key'),
    ];

    $this->checkKeyPairDependencies();

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config($this->getConfigName());
    foreach (['user', 'private_key'] as $key) {
      if (!$this->configIsOverridden("key_pair.$key")) {
        $config->set("key_pair.$key", $form_state->getValue($key));
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
