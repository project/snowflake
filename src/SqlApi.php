<?php

namespace Drupal\snowflake;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\State\StateInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\snowflake\Authenticator\AuthenticatorInterface;
use Drupal\snowflake\Authenticator\KeyPairAuthenticator;
use Drupal\snowflake\Authenticator\OAuthAuthenticator;
use Drupal\snowflake\Statement\Statements;
use Drupal\snowflake\StatementResult\CancelStatus;
use Drupal\snowflake\StatementResult\QueryFailureStatus;
use Drupal\snowflake\StatementResult\QueryStatus;
use Drupal\snowflake\StatementResult\ResultSet;
use Drupal\snowflake\StatementResult\StatusResultInterface;
use Drupal\snowflake\Token\TokenInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Snowflake SQL API client.
 *
 * @package Drupal\snowflake
 */
final class SqlApi {

  /**
   * Snowflake auth immutable config.
   */
  protected ImmutableConfig $authConfig;

  /**
   * Authenticator.
   */
  protected AuthenticatorInterface $authenticator;

  /**
   * Snowflake immutable config.
   */
  protected ImmutableConfig $config;

  /**
   * Config factory.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * HTTP Client.
   */
  protected ClientInterface $httpClient;

  /**
   * Key repository.
   */
  protected KeyRepositoryInterface $keyRepository;

  /**
   * State kv storage.
   */
  protected StateInterface $state;

  /**
   * Snowflake API token.
   */
  protected TokenInterface $token;

  /**
   * ApiClient constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, KeyRepositoryInterface $key_repository, StateInterface $state) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->keyRepository = $key_repository;
    $this->state = $state;
    $this->authConfig = $this->configFactory->get('snowflake.auth');
    $this->config = $this->configFactory->get('snowflake.settings');

    $parameters = [$this->authConfig, $http_client, $key_repository, $state];
    $this->authenticator = match($this->authConfig->get('method')) {
      'key_pair' => new KeyPairAuthenticator(...$parameters),
      'oauth' => new OAuthAuthenticator(...$parameters),
      NULL => throw new \RuntimeException("No authentication method has been configured. Configure an authentication method at /admin/config/snowflake/auth."),
      default => throw new \RuntimeException("Unknown authentication method {$this->authConfig->get('method')}.")
    };
    $this->token = $this->authenticator->getToken();
  }

  /**
   * Gets an API endpoint with the provided path.
   */
  protected function getEndpoint(string $path): string {
    $account_identifier = strtolower($this->authConfig->get('account_identifier'));
    return "https://$account_identifier.snowflakecomputing.com/api/v2/statements$path";
  }

  /**
   * Generates required headers for a query.
   */
  private function generateQueryHeaders(): array {
    return [
      'Accept' => 'application/json',
      'Authorization' => "Bearer {$this->token->getAccessToken()}",
      'User-Agent' => 'drupalSnowflake/1.0',
      'X-Snowflake-Authorization-Token-Type' => $this->token->getType(),
    ];
  }

  /**
   * Execute a group of statements.
   *
   * Documented response code return types:
   *  - 200 OK: Result set.
   *  - 202 ACCEPTED: Query status.
   *  - 408 TIMEOUT: Query failure status.
   *  - 422 UNPROCESSABLE ENTITY: Query failure status.
   *
   * All other 4XX or 5XX response codes will throw the HTTP client exception.
   *
   * @url https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#post-api-v2-statements
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function executeStatements(Statements $statements): ResultSet|StatusResultInterface {
    // Create statements payload.
    $payload = array_filter(array_merge($this->config->get('statements_defaults.settings'), $statements->getSettings()));
    $payload['statement'] = (string) $statements;
    $payload['bindings'] = $statements->getBindings();
    $payload['parameters'] = array_filter(array_merge($this->config->get('statements_defaults.parameters'), $statements->getParameters()));
    $payload['parameters']['MULTI_STATEMENT_COUNT'] = count($statements);

    // Create query options.
    $query = [
      'requestId' => $statements->getId(),
      'async' => $statements->isAsync() ? 'true' : NULL,
      'nullable' => $statements->isNullable() ? NULL : 'false',
    ];

    try {
      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->post($this->getEndpoint('/'), array_filter([
        'headers' => $this->generateQueryHeaders(),
        'json' => array_filter($payload),
        'query' => array_filter($query),
      ]));
      if ($response->getStatusCode() === Response::HTTP_ACCEPTED) {
        return new QueryStatus($response);
      }
      return new ResultSet($response);
    }
    catch (ClientException $e) {
      if ($e->getCode() === Response::HTTP_UNPROCESSABLE_ENTITY || $e->getCode() === Response::HTTP_REQUEST_TIMEOUT) {
        return new QueryFailureStatus($e->getResponse());
      }
      throw $e;
    }
  }

  /**
   * Get status of a statement by handle.
   *
   * Documented response code return types:
   *  - 200 OK: Result set.
   *  - 202 ACCEPTED: Query status.
   *  - 422 UNPROCESSABLE ENTITY: Query failure status.
   *
   * All other 4XX or 5XX response codes will throw the HTTP client exception.
   *
   * @url https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#get-api-v2-statements-statementhandle
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getStatementStatus(string $statement_handle, int $partition = 0): ResultSet|StatusResultInterface {
    $this->ensureUuid($statement_handle);
    try {
      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->get($this->getEndpoint("/$statement_handle"), [
        'headers' => $this->generateQueryHeaders(),
        'query' => ['partition' => $partition],
      ]);
      if ($response->getStatusCode() === Response::HTTP_ACCEPTED) {
        return new QueryStatus($response);
      }
      return new ResultSet($response);
    }
    catch (ClientException $e) {
      if ($e->getCode() === Response::HTTP_UNPROCESSABLE_ENTITY) {
        return new QueryFailureStatus($e->getResponse());
      }
      throw $e;
    }
  }

  /**
   * Cancel a running statement.
   *
   * Documented response code return types:
   *  - 200 OK: Cancel status.
   *  - 422 UNPROCESSABLE ENTITY: Query failure status.
   *
   * All other 4XX or 5XX response codes will throw the HTTP client exception.
   *
   * @url https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#post-api-v2-statements-statementhandle-cancel
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function cancelStatement(string $statement_handle): CancelStatus|QueryFailureStatus {
    $this->ensureUuid($statement_handle);
    try {
      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->post($this->getEndpoint("/$statement_handle/cancel"), [
        'headers' => $this->generateQueryHeaders(),
      ]);
      return new CancelStatus($response);
    }
    catch (ClientException $e) {
      if ($e->getCode() === Response::HTTP_UNPROCESSABLE_ENTITY) {
        return new QueryFailureStatus($e->getResponse());
      }
      throw $e;
    }
  }

  /**
   * Ensures a string is a UUID.
   */
  private function ensureUuid(string $uuid): void {
    if (!Uuid::isValid($uuid)) {
      throw new \InvalidArgumentException("$uuid is not a valid UUID.");
    }
  }

}
