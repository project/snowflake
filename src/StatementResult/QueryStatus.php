<?php

namespace Drupal\snowflake\StatementResult;

use Drupal\snowflake\StatementResult\Traits\HasStatementStatusUrlTrait;
use GuzzleHttp\Psr7\Response;

/**
 * Query status result type (202).
 *
 * Snowflake uses this type for certain 408 and 422 responses as well but those
 * may be missing properties otherwise guaranteed to exist on this type, so they
 * have more specific classes.
 *
 * @url https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#querystatus
 */
final class QueryStatus extends StatementResultBase implements StatusResultInterface {

  use HasStatementStatusUrlTrait;

  /**
   * Constructs a QueryStatus from an API response.
   */
  public function __construct(Response $response) {
    parent::__construct($response);
    $results = $this->getRawResults();
    $this->statementStatusUrl = $results->statementStatusUrl;
  }

}
