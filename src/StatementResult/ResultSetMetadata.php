<?php

namespace Drupal\snowflake\StatementResult;

/**
 * Representation of statement result set metadata.
 *
 * @url https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#resultset-resultsetmetadata
 */
final class ResultSetMetadata {

  /**
   * Result format.
   */
  protected string $format;

  /**
   * Number of rows in the result.
   */
  protected int $numRows;

  /**
   * Partition info.
   */
  protected array $partitionInfo;

  /**
   * Row type data for each column.
   */
  protected array $rowType = [];

  /**
   * Constructs a ResultSetMetadata instance.
   */
  public function __construct(object $metadata) {
    $this->format = $metadata->format;
    $this->numRows = $metadata->numRows;
    $this->partitionInfo = $metadata->partitionInfo;
    foreach ($metadata->rowType as $metadata) {
      $this->rowType[] = new ColumnMetadata($metadata);
    }
  }

  /**
   * Logical alias for getting "row type" metadata.
   *
   * @return \Drupal\snowflake\StatementResult\ColumnMetadata[]
   *   Metadata for each column in the result rows.
   */
  public function getColumnMetadata(): array {
    return $this->getRowType();
  }

  /**
   * Gets the result format.
   */
  public function getFormat(): string {
    return $this->format;
  }

  /**
   * Gets the number of rows in the result.
   */
  public function getNumRows(): int {
    return $this->numRows;
  }

  /**
   * Gets partition info.
   *
   * @return array
   *   Partition info. Each array object has up to three properties:
   *    - rowCount,
   *    - uncompressedSize, and
   *    - compressedSize.
   */
  public function getPartitionInfo(): array {
    return $this->partitionInfo;
  }

  /**
   * Gets the result columns type data.
   *
   * @return \Drupal\snowflake\StatementResult\ColumnMetadata[]
   *   Metadata for each column in the result rows.
   */
  public function getRowType(): array {
    return $this->rowType;
  }

}
