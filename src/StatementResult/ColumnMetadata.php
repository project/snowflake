<?php

namespace Drupal\snowflake\StatementResult;

/**
 * Metadata for a column included in result sets metadata.
 *
 * @url https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#resultset-resultsetmetadata
 */
final class ColumnMetadata {

  /**
   * Column byte length.
   */
  protected ?int $byteLength = NULL;

  /**
   * Database collation.
   */
  protected ?string $collation = NULL;

  /**
   * Database the column belongs to.
   */
  protected string $database;

  /**
   * Length of the column.
   */
  protected ?int $length = NULL;

  /**
   * Name of the column.
   */
  protected string $name;

  /**
   * Whether the column is nullable.
   */
  protected bool $nullable;

  /**
   * Precision of the column.
   */
  protected ?int $precision = NULL;

  /**
   * Scale of the column.
   */
  protected ?int $scale = NULL;

  /**
   * Schema name.
   */
  protected string $schema;

  /**
   * Table name.
   */
  protected string $table;

  /**
   * Snowflake data type of the column.
   */
  protected string $type;

  /**
   * Constructs a ColumnMetadata instance.
   */
  public function __construct(object $metadata) {
    $this->byteLength = $metadata->byteLength;
    $this->collation = $metadata->collation;
    $this->database = $metadata->database;
    $this->length = $metadata->length;
    $this->name = $metadata->name;
    $this->nullable = $metadata->nullable;
    $this->precision = $metadata->precision;
    $this->scale = $metadata->scale;
    $this->schema = $metadata->schema;
    $this->table = $metadata->table;
    $this->type = $metadata->type;
  }

  /**
   * Gets the byte length of the column.
   */
  public function getByteLength(): ?int {
    return $this->byteLength;
  }

  /**
   * Gets the database collation.
   */
  public function getCollation(): ?string {
    return $this->collation;
  }

  /**
   * Gets the database name.
   */
  public function getDatabase(): string {
    return $this->database;
  }

  /**
   * Gets the length of the column.
   */
  public function getLength(): ?int {
    return $this->length;
  }

  /**
   * Gets the name of the column.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Indicates whether the column is nullable.
   */
  public function isNullable(): bool {
    return $this->nullable;
  }

  /**
   * Gets the precision of the column.
   */
  public function getPrecision(): ?int {
    return $this->precision;
  }

  /**
   * Gets the scale of the column.
   */
  public function getScale(): ?int {
    return $this->scale;
  }

  /**
   * Gets the schema name.
   */
  public function getSchema(): string {
    return $this->schema;
  }

  /**
   * Gets the table name.
   */
  public function getTable(): string {
    return $this->table;
  }

  /**
   * Gets the Snowflake data type of the column.
   *
   * @url https://docs.snowflake.com/en/sql-reference/intro-summary-data-types.html
   */
  public function getType(): string {
    return $this->type;
  }

}
