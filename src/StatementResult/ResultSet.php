<?php

namespace Drupal\snowflake\StatementResult;

use Drupal\snowflake\StatementResult\Traits\HasCreatedOnTrait;
use Drupal\snowflake\StatementResult\Traits\HasSqlStateTrait;
use Drupal\snowflake\StatementResult\Traits\HasStatementStatusUrlTrait;
use GuzzleHttp\Psr7\Response;

/**
 * Result set result type.
  */
final class ResultSet extends StatementResultBase {

  use HasCreatedOnTrait;
  use HasSqlStateTrait;
  use HasStatementStatusUrlTrait;

  /**
   * Result set data.
   */
  protected array $data;

  /**
   * Metadata about the result set.
   */
  protected ResultSetMetadata $metadata;

  /**
   * Unique identifiers for the statements being executed for this request.
   *
   * Only present when multiple statements are executed.
   */
  protected ?array $statementHandles = NULL;

  /**
   * Result set stats (when changes are made).
   */
  protected ?ResultSetStats $stats = NULL;

  /**
   * Constructs a ResultSet from an API response.
   */
  public function __construct(Response $response) {
    parent::__construct($response);
    $results = $this->getRawResults();
    $this->createdOn = $results->createdOn;
    $this->data = $results->data;
    $this->metadata = new ResultSetMetadata($results->resultSetMetaData);
    $this->statementHandles = $results->statementHandles ?? NULL;
    $this->statementStatusUrl = $results->statementStatusUrl;
    if ($results->stats ?? FALSE) {
      $this->stats = new ResultSetStats($results->stats);
    }
  }

  /**
   * Gets the results data.
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Gets metadata about the result set.
   */
  public function getMetadata(): ResultSetMetadata {
    return $this->metadata;
  }

  /**
   * Gets unique identifiers for the statements being executed for this request.
   */
  public function getStatementHandles(): ?array {
    return $this->statementHandles;
  }

  /**
   * Get result set stats (when changes are made).
   */
  public function getStats(): ?ResultSetStats {
    return $this->stats;
  }

}
