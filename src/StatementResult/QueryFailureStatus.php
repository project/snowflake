<?php

namespace Drupal\snowflake\StatementResult;

use Drupal\snowflake\StatementResult\Traits\HasSqlStateTrait;
use GuzzleHttp\Psr7\Response;

/**
 * Query failure result type (408 or 422).
 *
 * Snowflake differentiates the query status objects for 408 and 422 in
 * documentation however in practice they appear to have the same properties.
 *
 * @url https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#queryfailurestatus
 */
final class QueryFailureStatus extends StatementResultBase implements StatusResultInterface {

  use HasSqlStateTrait;

  /**
   * Constructs a QueryFailureStatus from an API response.
   */
  public function __construct(Response $response) {
    parent::__construct($response);
    $results = $this->getRawResults();
    $this->sqlState = $results->sqlState;
  }

}
