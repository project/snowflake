<?php

namespace Drupal\snowflake\StatementResult;

/**
 * Representation of statement result stats.
 *
 * @url https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#label-sql-api-reference-resultset-stats
 */
final class ResultSetStats {

  /**
   * Number of rows deleted.
   */
  protected int $deleted;

  /**
   * Number of duplicate rows updated.
   */
  protected int $duplicateUpdated;

  /**
   * Number of rows inserted.
   */
  protected int $inserted;

  /**
   * Number of rows updated.
   */
  protected int $updated;

  /**
   * Constructs a ResultSetStats instance.
   */
  public function __construct(object $metadata) {
    $this->deleted = $metadata->numRowsDeleted;
    $this->duplicateUpdated = $metadata->numDuplicateRowsUpdated;
    $this->inserted = $metadata->numRowsInserted;
    $this->updated = $metadata->numRowsUpdated;
  }

  /**
   * Gets number of rows deleted.
   */
  public function getDeleted(): int {
    return $this->deleted;
  }

  /**
   * Gets number of duplicate rows updated.
   */
  public function getDuplicateUpdated(): int {
    return $this->duplicateUpdated;
  }

  /**
   * Gets number of rows inserted.
   */
  public function getInserted(): int {
    return $this->inserted;
  }

  /**
   * Gets number of rows updated.
   */
  public function getUpdated(): int {
    return $this->updated;
  }

}
