<?php

namespace Drupal\snowflake\StatementResult;

use Drupal\snowflake\StatementResult\Traits\HasSqlStateTrait;
use GuzzleHttp\Psr7\Response;

/**
 * Cancel status result type (200).
 *
 * @url https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#cancelstatus
 */
final class CancelStatus extends StatementResultBase implements StatusResultInterface {

  use HasSqlStateTrait;

  /**
   * Constructs a CancelStatus from an API response.
   */
  public function __construct(Response $response) {
    parent::__construct($response);
    $results = $this->getRawResults();
    $this->sqlState = $results->sqlState;
  }

}
