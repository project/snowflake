<?php

namespace Drupal\snowflake\StatementResult;

use GuzzleHttp\Psr7\Response;

/**
 * Base class for all statement results.
 */
abstract class StatementResultBase {

  /**
   * Code.
   */
  protected string $code;

  /**
   * Message.
   */
  protected string $message;

  /**
   * Raw response from the API.
   */
  protected Response $response;

  /**
   * Statement handle (UUID).
   */
  protected string $statementHandle;

  /**
   * Constructs a StatementResultBase from an API response.
   */
  public function __construct(Response $response) {
    $this->response = $response;
    $results = $this->getRawResults();
    $this->code = $results->code;
    $this->message = $results->message;
    $this->statementHandle = $results->statementHandle;
  }

  /**
   * Get the statement response code.
   */
  public function getCode(): string {
    return $this->code;
  }

  /**
   * Get the statement response message.
   */
  public function getMessage(): string {
    return $this->message;
  }

  /**
   * Gets the raw results from the response.
   */
  public function getRawResults(): object {
    return json_decode($this->response->getBody());
  }

  /**
   * Gets the raw response from the API.
   */
  public function getResponse(): Response {
    return $this->response;
  }

  /**
   * Get the statement handle.
   */
  public function getStatementHandle(): string {
    return $this->statementHandle;
  }

}
