<?php

namespace Drupal\snowflake\StatementResult\Traits;

/**
 * Adds result created on methods.
 */
trait HasCreatedOnTrait {

  /**
   * Timestamp that specifies when the statement execution started.
   *
   * The timestamp is expressed in milliseconds since the epoch.
   */
  protected int $createdOn;

  /**
   * Get the created on timestamp.
   */
  public function getCreatedOn(): int {
    return $this->createdOn;
  }

}
