<?php

namespace Drupal\snowflake\StatementResult\Traits;

/**
 * Adds statement status URL methods.
 */
trait HasStatementStatusUrlTrait {

  /**
   * Statement status URL.
   */
  protected string $statementStatusUrl;

  /**
   * Get the statement status URL.
   */
  public function getStatementStatusUrl(): string {
    return $this->statementStatusUrl;
  }

}
