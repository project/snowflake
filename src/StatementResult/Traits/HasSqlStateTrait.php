<?php

namespace Drupal\snowflake\StatementResult\Traits;

/**
 * Adds statement SQL state methods.
 */
trait HasSqlStateTrait {

  /**
   * SQL state code.
   */
  protected string $sqlState;

  /**
   * Get the statement SQL state code.
   */
  public function getSqlState(): string {
    return $this->sqlState;
  }

}
