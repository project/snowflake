<?php

declare(strict_types = 1);

namespace Drupal\snowflake\Statement;

/**
 * Representation of a single statement.
 */
final class Statement {

  /**
   * SQL statement.
   */
  protected string $statement;

  /**
   * Bindings for the statement.
   */
  protected array $bindings = [];

  /**
   * Supported variable binding types.
   */
  public const BINDING_TYPES = [
    'BINARY',
    'BOOLEAN',
    'DATE',
    'FIXED',
    'REAL',
    'TEXT',
    'TIME',
    'TIMESTAMP_LTZ',
    'TIMESTAMP_NTZ',
    'TIMESTAMP_TZ',
  ];

  /**
   * Constructs a Statement instance.
   */
  public function __construct(string $statement) {
    $this->statement = $statement;
  }

  /**
   * Creates a new Statement instance.
   */
  public static function create(string $statement): Statement {
    return new Statement($statement);
  }

  /**
   * Gets the statement query.
   */
  public function getStatement(): string {
    return $this->statement;
  }

  /**
   * Gets the statement's bindings.
   */
  public function getBindings(): array {
    return $this->bindings;
  }

  /**
   * Adds a binding (order matters).
   *
   * The password value will always be converted to a string.
   */
  public function addBinding(string $type, string|bool|int|float $value): Statement {
    if (!in_array($type, self::BINDING_TYPES)) {
      throw new \InvalidArgumentException("Binding type $type not supported. Valid types: " . implode(',', self::BINDING_TYPES));
    }
    // Convert TRUE and FALSE to "true" and "false" instead of 1 and 0.
    if (is_bool($value)) {
      $value = json_encode($value);
    }
    $this->bindings[] = ['type' => $type, 'value' => (string) $value];
    return $this;
  }

  /**
   * Gets the statement as a string.
   */
  public function __toString(): string {
    return $this->getStatement();
  }

}
