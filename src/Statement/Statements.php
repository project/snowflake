<?php

namespace Drupal\snowflake\Statement;

use Drupal\Component\Uuid\Uuid;

/**
 * Representation of statements for a Snowflake SQL API query.
 */
final class Statements implements \Countable {

  /**
   * Supported session parameters for all statements.
   *
   * @url https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#statements-parameters
   */
  public const PARAMETERS = [
    'BINARY_OUTPUT_FORMAT',
    'DATE_OUTPUT_FORMAT',
    'QUERY_TAG',
    'TIME_OUTPUT_FORMAT',
    'TIMESTAMP_LTZ_OUTPUT_FORMAT',
    'TIMESTAMP_NTZ_OUTPUT_FORMAT',
    'TIMESTAMP_OUTPUT_FORMAT',
    'TIMESTAMP_TZ_OUTPUT_FORMAT',
    'TIMEZONE',
  ];

  /**
   * Supported settings for all statements.
   *
   * @url https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#body-of-the-post-request-to-api-v2-statements
   */
  public const SETTINGS = [
    'timeout',
    'database',
    'schema',
    'warehouse',
    'role',
  ];

  /**
   * Whether to run the queries asynchronously.
   */
  protected bool $async = FALSE;


  /**
   * Custom query ID (must be a UUID).
   */
  protected ?string $id = NULL;

  /**
   * Whether statements results should use NULL or the string "null".
   */
  protected bool $nullable = TRUE;

  /**
   * Session parameters for all statements.
   */
  protected array $parameters = [];

  /**
   * Settings for all statements.
   */
  protected array $settings = [];

  /**
   * SQL statements.
   *
   * @var \Drupal\snowflake\Statement\Statement[]
   */
  protected array $statements;

  /**
   * Constructs a Statements instance.
   */
  public function __construct(Statement ...$statements) {
    $this->statements = $statements;
  }

  /**
   * Creates a new Statements instance.
   */
  public static function create(Statement ...$statements): Statements {
    return new Statements(...$statements);
  }

  /**
   * Gets all statements as a multi-statement string.
   */
  public function __toString(): string {
    return implode('; ', $this->statements);
  }

  /**
   * {@inheritdoc}
   */
  public function count(): int {
    return count($this->statements);
  }

  /**
   * Adds a parameter for all statements.
   *
   * Upper case is enforced on keys to match expectations for the query payload.
   */
  public function addParameter(string $key, string $value): Statements {
    $key = strtoupper($key);
    if (!in_array($key, self::PARAMETERS)) {
      throw new \InvalidArgumentException("Parameter $key not supported. Supported parameters: " . implode(',', self::PARAMETERS));
    }
    $this->parameters[$key] = $value;
    return $this;
  }

  /**
   * Adds a setting for all statements.
   */
  public function addSetting(string $key, string|int $value): Statements {
    $key = strtolower($key);
    if (!in_array($key, self::SETTINGS)) {
      throw new \InvalidArgumentException("Setting $key not supported. Supported settings: " . implode(',', self::SETTINGS));
    }
    $this->settings[$key] = $value;
    return $this;
  }

  /**
   * Adds a statement to the statements.
   */
  public function addStatement(Statement $statement): Statements {
    $this->statements[] = $statement;
    return $this;
  }

  /**
   * Combines bindings from all statements in order.
   *
   * Bindings are adjusted to be 1-indexed to match expectations of the
   * statements API.
   */
  public function getBindings(): array {
    $bindings = [NULL];
    foreach ($this->statements as $statement) {
      $bindings = array_merge($bindings, $statement->getBindings());
    }
    unset($bindings[0]);
    return $bindings;
  }

  /**
   * Gets statements ID.
   */
  public function getId(): ?string {
    return $this->id;
  }

  /**
   * Gets session parameters.
   */
  public function getParameters(): array {
    return $this->parameters;
  }

  /**
   * Gets statements settings.
   */
  public function getSettings(): array {
    return $this->settings;
  }

  /**
   * Whether the statements will run asynchronously.
   */
  public function isAsync(): bool {
    return $this->async;
  }

  /**
   * Whether the statements results will use NULLs or string "null" values.
   */
  public function isNullable(): bool {
    return $this->nullable;
  }

  /**
   * Whether to run the queries asynchronously.
   */
  public function setAsync(bool $async): Statements {
    $this->async = $async;
    return $this;
  }

  /**
   * Sets an ID (UUID) for the statements.
   */
  public function setId(string $id): Statements {
    if (!Uuid::isValid($id)) {
      throw new \InvalidArgumentException("ID $id is not a valid UUID. Statements ID must be a valid UUID.");
    }
    $this->id = $id;
    return $this;
  }

  /**
   * Sets nullable setting for NULL values in statements results.
   */
  public function setNullable(bool $nullable): Statements {
    $this->nullable = $nullable;
    return $this;
  }

}
