<?php

namespace Drupal\snowflake\Authenticator;

use Drupal\snowflake\Token\Token;
use Drupal\snowflake\Token\TokenInterface;
use Firebase\JWT\JWT;

/**
 * Key Pair authenticator.
 */
final class KeyPairAuthenticator extends AuthenticatorBase {

  /**
   * {@inheritdoc}
   */
  public static function getTokenType(): string {
    return 'keypair';
  }

  /**
   * {@inheritdoc}
   */
  public function refreshToken(): TokenInterface {
    $time = \Drupal::time()->getCurrentTime();
    $expiration = $time + 60 * 45;
    $account = strtoupper($this->authConfig->get('account_identifier'));
    $user = strtoupper($this->authConfig->get('key_pair.user'));

    // Get public key from private key.
    $key = $this->keyRepository->getKey($this->authConfig->get('key_pair.private_key'))->getKeyValue();
    $private_key_pem = openssl_pkey_get_private($key);
    $public_key_pem = openssl_pkey_get_details($private_key_pem)['key'];

    // Convert PEM to DER.
    // @url https://pumka.net/2009/12/19/reading-writing-and-converting-rsa-keys-in-pem-der-publickeyblob-and-privatekeyblob-formats/
    $lines = explode("\n", trim($public_key_pem));
    unset($lines[count($lines) - 1]);
    unset($lines[0]);
    $public_key_der = base64_decode(implode('', $lines));

    // Generate fingerprint and encode token.
    // @url https://docs.snowflake.com/en/developer-guide/sql-api/authenticating.html#using-key-pair-authentication
    $fingerprint = base64_encode(hash('sha256', $public_key_der, TRUE));
    $jwt = JWT::encode([
      'iss' => "$account.$user.SHA256:$fingerprint",
      'sub' => "$account.$user",
      // @todo Figure out how to ensure this is the Snowflake timezone.
      'iat' => $time,
      // Maximum expiration time is one hour.
      'exp' => $expiration,
    ], $private_key_pem, 'RS256');

    $token = new Token($jwt, $expiration, self::getTokenType());
    $this->state->set($this->buildTokenStorageKey(), $token);
    return $token;
  }

}
