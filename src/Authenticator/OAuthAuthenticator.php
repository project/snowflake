<?php

namespace Drupal\snowflake\Authenticator;

use Drupal\snowflake\Token\Token;
use Drupal\snowflake\Token\TokenInterface;

/**
 * OAuth authenticator.
 */
final class OAuthAuthenticator extends AuthenticatorBase {

  /**
   * {@inheritdoc}
   */
  public function getToken(): TokenInterface {
    // @todo Implement getToken() method.
    return new Token('TODO', -1, self::getTokenType());
  }

  /**
   * {@inheritdoc}
   */
  public static function getTokenType(): string {
    return 'oauth';
  }

  /**
   * {@inheritdoc}
   */
  public function refreshToken(): TokenInterface {
    // @todo Implement refreshToken() method.
    return new Token('TODO', -1, self::getTokenType());
  }

}
