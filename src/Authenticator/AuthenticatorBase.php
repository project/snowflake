<?php

namespace Drupal\snowflake\Authenticator;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\State\StateInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\snowflake\Token\TokenInterface;
use GuzzleHttp\ClientInterface;

/**
 * Base class for authenticators.
 */
abstract class AuthenticatorBase implements AuthenticatorInterface {

  /**
   * Snowflake auth immutable config.
   */
  protected ImmutableConfig $authConfig;

  /**
   * HTTP Client.
   */
  protected ClientInterface $httpClient;

  /**
   * Key repository.
   */
  protected KeyRepositoryInterface $keyRepository;

  /**
   * State kv storage.
   */
  protected StateInterface $state;

  /**
   * AuthenticatorBase constructor.
   */
  public function __construct(ImmutableConfig $auth_config, ClientInterface $http_client, KeyRepositoryInterface $key_repository, StateInterface $state) {
    $this->authConfig = $auth_config;
    $this->httpClient = $http_client;
    $this->keyRepository = $key_repository;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function buildTokenStorageKey(): string {
    $type = $this->getTokenType();
    return "snowflake.auth.$type.token";
  }

  /**
   * {@inheritdoc}
   */
  public function getToken(): TokenInterface {
    /** @var \Drupal\snowflake\Token\TokenInterface $token */
    $token = $this->state->get($this->buildTokenStorageKey());
    if (!$token || $token->isExpired()) {
      $token = $this->refreshToken();
    }
    return $token;
  }

}
