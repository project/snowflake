<?php

namespace Drupal\snowflake\Authenticator;

use Drupal\snowflake\Token\TokenInterface;

/**
 * Base Snowflake authenticator interface.
 */
interface AuthenticatorInterface {

  /**
   * Builds a string for identifying the key (e.g., in a K/V store).
   */
  public function buildTokenStorageKey(): string;

  /**
   * Get a token, refreshing if necessary.
   */
  public function getToken(): TokenInterface;

  /**
   * Get the type of token.
   */
  public static function getTokenType(): string;

  /**
   * Force a refresh of the active token and return the fresh token.
   */
  public function refreshToken(): TokenInterface;

}
