CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Usage
* Support requests
* Maintainers


INTRODUCTION
------------

Snowflake provides a service for interaction with the [Snowflake SQL API](https://docs.snowflake.com/en/developer-guide/sql-api/index.html).
Note the SQL API is different from the [PHP PDO Driver for Snowflake](https://docs.snowflake.com/en/user-guide/php-pdo-driver.html)
and this module does not currently support the PHP PDO driver.


REQUIREMENTS
------------

This module requires the following modules:

* [Key](https://www.drupal.org/project/key) - For storing API access
  credentials.

At least one additional library is required depending on the authentication
method the module will use.

* For Key Pair authentication install [`firebase/php-jwt`](https://packagist.org/packages/firebase/php-jwt):

      composer require firebase/php-jwt


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See
[Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
for details.


CONFIGURATION
-------------

Configure authentication settings by navigating to Configuration > Snowflake >
Snowflake Authentication (`/admin/config/snowflake/auth`). Currently, the only
supported authentication method is [Key Pair Authentication](https://docs.snowflake.com/en/developer-guide/sql-api/authenticating.html#using-key-pair-authentication).

Configure default [settings](https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#body-of-the-post-request-to-api-v2-statements)
and [parameters](https://docs.snowflake.com/en/developer-guide/sql-api/reference.html#label-sql-api-reference-statements-parameters)
for SQL statements by navigating to Configuration > Snowflake > Snowflake
Settings (`/admin/config/snowflake/settings`).


USAGE
------------

The Snowflake module provides a single service -- the Snowflake SQL API client.

```php
/** @var \Drupal\snowflake\SqlApi $client */
$client = Drupal::service('snowflake.sql_api');
```

Create statements with the `Drupal\snowflake\Statement\Statement` class.

```php
$statement1 = Drupal\snowflake\Statement\Statement::create(
  'SELECT FIRST_NAME, LAST_NAME FROM CONTACTS LIMIT 10'
);
$statement2 = Drupal\snowflake\Statement\Statement::create(
  'SELECT FIRST_NAME, LAST_NAME FROM CONTACTS WHERE USER_ID=?'
)->addBinding('TEXT', '1234567890');
$statement3 = Drupal\snowflake\Statement\Statement::create(
  'CALL my_stored_procedure()
');
```

Execute a single statement or combine multiple statements with the
`Drupal\snowflake\Statement\Statements` class.

```php
/** @var \Drupal\snowflake\SqlApi $client */
$client = Drupal::service('snowflake.sql_api');

try {
  // Execute a single statement.
  $statements = Drupal\snowflake\Statement\Statements::create($statement1);
  $result = $client->executeStatements($statements);
  if ($result instanceof \Drupal\snowflake\StatementResult\ResultSet) {
    $data = $result->getData();
    $metadata = $result->getMetadata();
  }

  // Combine multiple statements, and run asynchronously.
  $statements = new Drupal\snowflake\Statement\Statements();
  $statements->addStatement($statement1)
    ->addStatement($statement2)
    ->addStatement($statement3)
    ->setAsync(TRUE);
  $result = $client->executeStatements($statements);
  $status = $client->getStatementStatus($result->getStatementHandle());
  foreach ($status->getStatementHandles() as $handle) {
    $statement_status = $client->getStatementStatus($handle);
    $data = $statement_status->getData();
  }
}
catch (\GuzzleHttp\Exception\BadResponseException $e) {
  watchdog_exception('my_module', $e);
}
```


SUPPORT REQUESTS
----------------

* Before posting a support request, carefully read the installation
  instructions provided in module documentation page.

* Before posting a support request, check the Recent Log entries at
  `/admin/reports/dblog`.

* Once you have done this, you can post a support request at module issue
  queue: [https://www.drupal.org/project/issues/snowflake](https://www.drupal.org/project/issues/snowflake)

* When posting a support request, please inform if you were able to see any
  errors in the Recent Log entries.


MAINTAINERS
-----------

Current maintainers:

* [Christopher C. Wells (wells)](https://www.drupal.org/u/wells)

Development sponsored by:

* [Cascade Public Media](https://www.drupal.org/cascade-public-media)
